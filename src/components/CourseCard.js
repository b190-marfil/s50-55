import { Row, Col, Card, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';


export default function CourseCard({courseProp}){
	console.log(courseProp);
	console.log(typeof courseProp);

	const {name,description,price} = courseProp;

	const[count,setCount] = useState(0);
	const[seats,setSeats] = useState(10);
	const [isOpen,setIsOpen] = useState(true);

// ACTIVITY S51
	function enroll(){
		setCount(count + 1);
		setSeats(seats - 1);
		console.log(`Enrollees: ${count}`);
		console.log(`Seats: ${seats}`);
	}

	useEffect(()=>{
		if(seats === 0){
			setIsOpen(false);
		}
	},[seats]);

	return(
		<Row>
			<Col>
				<Card>
					<Card.Body>
						<Card.Title>
							<h5>{name}</h5>
						</Card.Title>

						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>

						<Card.Text>Enrollees:{count}</Card.Text>
						{isOpen ? 
						<Button variant="primary" onClick={enroll}>Enroll</Button>
						:
						<Button variant="primary" onClick={enroll} disabled>Enroll</Button>
						}
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}