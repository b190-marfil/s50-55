import './App.css';

import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

// Web Components
import AppNavbar from './AppNavbar';

// Web Pages
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import ErrorPage from "./pages/ErrorPage";



function App() {
  return (
    <Fragment>
      <Router>
      <AppNavbar />
        <Container fluid>
          <Routes>
            <Route path="*" element={<ErrorPage />} />
            <Route path='/' element={<Home />} />
            <Route path='/courses' element={<Courses />} />
            <Route path='/login' element={<Login />} />
            <Route path='/register' element={<Register />} />
            <Route path='/logout' element={<Logout />} />
          </Routes>
        </Container>
      </Router>
    </Fragment>
  );
}

export default App;