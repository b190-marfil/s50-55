import { Button, Row, Col } from "react-bootstrap";

export default function ErrorPage(){
	return(
		<Row>
			<Col className="p-5">
				<h1>Page Not Found</h1>
				<p>Go back to the <a href="/#home">homepage</a></p>
			</Col>
		</Row>
	)
}