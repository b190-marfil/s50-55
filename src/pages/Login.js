import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Login(){
	const [email,setEmail] = useState("");
	const [password1,setPassword1] = useState("");
	const [isActive,setIsActive] = useState(false);

	console.log(email);
	console.log(password1);

	useEffect(() =>{
		if(email !== "" && password1 !== "")
		{
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	},[email,password1])

	function Login(e){
		e.preventDefault();

		localStorage.setItem('email',email);

		setEmail("");
		setPassword1("");

		alert(`You are now logged in.`);
	}

	return (
	    <Form onSubmit={(e) => Login(e)}>
		<h1>Login</h1>
	      <Form.Group controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        	type="email" 
	        	placeholder="Enter email" 
	        	value= {email} 
	        	onChange={e =>setEmail(e.target.value)} 
	        	required
	        />
	      </Form.Group>

	      <Form.Group controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Password" 
	        	value= {password1} 
	        	onChange={e => setPassword1(e.target.value)}
	        	required
	        />
	      </Form.Group>

	      {isActive ?
	      <Button variant="success" type="submit" id="submitBtn">
	        Submit
	      </Button>
	      :
	      <Button variant="success" type="submit" id="submitBtn" disabled>
	        Submit
	      </Button>
	  	  }
	    </Form>
  	);
}