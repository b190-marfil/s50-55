const coursesData = [
	{
		id:"wdc-001",
		name: "PHP-Laravel",
		description: "Sed eu lectus ultricies, placerat est non, scelerisque leo. Nam tempor nibh eget efficitur tincidunt. Praesent quis dui auctor, posuere sapien eu, viverra nisi. Nunc nec tortor non mauris posuere pellentesque. Suspendisse potenti.",
		price: 45000,
		onOffer: true
	},{
		id:"wdc-002",
		name: "Python-Django",
		description: "Sed eu lectus ultricies, placerat est non, scelerisque leo. Nam tempor nibh eget efficitur tincidunt. Praesent quis dui auctor, posuere sapien eu, viverra nisi. Nunc nec tortor non mauris posuere pellentesque. Suspendisse potenti.",
		price: 45000,
		onOffer: true
	},{
		id:"wdc-003",
		name: "Java-Springboot",
		description: "Sed eu lectus ultricies, placerat est non, scelerisque leo. Nam tempor nibh eget efficitur tincidunt. Praesent quis dui auctor, posuere sapien eu, viverra nisi. Nunc nec tortor non mauris posuere pellentesque. Suspendisse potenti.",
		price: 45000,
		onOffer: true
	}
]

export default coursesData;